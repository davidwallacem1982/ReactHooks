import React from 'react';
import './App.css';

import Usuarios from './components/Usuarios/Usuarios'

const App = () => {
  return (
    <div className="App">
      <main>
        <Usuarios />
      </main>
    </div>
  );
}

export default App;
